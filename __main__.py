import os.path as path
import sys
import argparse

debug_options = ['info', 'warning', 'error', 'debug']

from src.deliver import *
from src.test import *
from src.install import *
from src.build import *



class Argparser:
    def __init__(self):
        arg_parser = argparse.ArgumentParser()

        arg_parser.add_argument('-V', '--version', action='version', version=self.get_tool_version())

        subprasers = arg_parser.add_subparsers(dest='command')

        ######################
        # DELIVER ARGSPARSER #
        ######################
        deliver = subprasers.add_parser('deliver', help='deliver framework with submodules')

        ###################
        # TEST ARGSPARSER #
        ###################
        test = subprasers.add_parser('test', help='test submodules')


        ######################
        # INSTALL ARGSPARSER #
        ######################
        install = subprasers.add_parser('install', help='install all submodules')

        ####################
        # BUILD ARGSPARSER #
        ####################
        build = subprasers.add_parser('build', help='build framework and submodules')

        self.args = arg_parser.parse_args()
        if hasattr(self, self.args.command):
            getattr(self,  self.args.command)()

    def get_tool_version(self):
        return "version"

    def deliver(self):
        deliver()

    def install(self):
        install()

    def build(self):
        build()

    def test(self):
        test()


class Argparser_:
    def __init__(self):

        choices = ['deliver', 'install', 'test', 'build']  # choices for the first argument
        arg_parser = argparse.ArgumentParser()

        arg_parser.add_argument('-V', '--version', action='version', version=self.version())
        arg_parser.add_argument("command", choices=choices)

        args = arg_parser.parse_args(sys.argv[1:2])
        if hasattr(self, args.command):
            getattr(self, args.command)()
        else:
            arg_parser.error('unknown command: %s' % args.command)

    def deliver(self):
        # create a new parser just for deliver
        arg_parser = argparse.ArgumentParser()

        # todo: get module list from parsing all modules and get the choices

        choices = ['complete', 'tests_build', 'forgot']
        arg_parser.add_argument('-o', '--option', default='complete', type=list, action='store',
                                help='enter one of the following options: %s' % choices)
        self.debug(arg_parser)
        args = arg_parser.parse_args(sys.argv[2:])

        deliver()

    def install(self):
        # create a new parser just for deliver
        arg_parser = argparse.ArgumentParser()

        # todo: get module list from parsing all modules and get the choices

        #arg_parser.add_argument('-s', '--selection', default=self.module_list, type=list, action='store',
        #                        help='enter the modules you want to test (default: all). %s' % self.module_list)
        self.debug(arg_parser)
        args = arg_parser.parse_args(sys.argv[2:])
        install()

    def test(self):
        # create a new parser just for deliver
        arg_parser = argparse.ArgumentParser()

        # todo: get module list from parsing all modules and get the choices

        arg_parser.add_argument('-s', '--selection', default=self.module_list, type=list, action='store',
                                help='enter the modules you want to test (default: all). %s' % self.module_list)
        self.debug(arg_parser)
        args = arg_parser.parse_args(sys.argv[2:])
        test()

    def build(self):
        # create a new parser just for deliver
        arg_parser = argparse.ArgumentParser()

        # todo: get module list from parsing all modules and get the choices

        #arg_parser.add_argument('-s', '--selection', default=self.module_list, type=list, action='store',
        #                        help='enter the modules you want to test (default: all). %s' % self.module_list)
        self.debug(arg_parser)
        args = arg_parser.parse_args(sys.argv[2:])
        build()

    def version(self):
        return 'version'
        # todo: get version number of tool

    def debug(self, arg_parser):
        '''

        add debug option to current argparser

        :param arg_parser: current argparser
        :return:
        '''
        arg_parser.add_argument('-d', '--debug_level', choices=debug_options, default='error',
                                help='select debug level (default:error). %s' % debug_options)


if __name__ == "__main__":
    Argparser()
    exit(0)
