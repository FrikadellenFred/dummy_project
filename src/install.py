import pyfiglet

def install():
    message = 'install'
    ascii_banner = pyfiglet.figlet_format(message, font='starwars')
    print(ascii_banner)