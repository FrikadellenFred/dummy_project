import pyfiglet

def deliver():
    message = 'deliver'
    ascii_banner = pyfiglet.figlet_format(message, font='starwars')
    print(ascii_banner)