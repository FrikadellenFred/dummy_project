import pyfiglet

def build():
    message = 'build'
    ascii_banner = pyfiglet.figlet_format(message, font='starwars')
    print(ascii_banner)